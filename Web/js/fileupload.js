﻿function fileupload() {
    var fileinput = $("input:last", $("#fileupload").next("span"));
    var formData = new FormData();
    formData.append('file', fileinput[0].files[0]);
    $.ajax({
        url: "/Common/UploadImage",
        type: "post",
        data: formData,
        processData: false, //告诉jQuery不要去处理发送的数据
        contentType: false, //告诉jQuery不要去设置Content-Type请求头
        success: function (data) {
            if (data.IsOK)
                $("#hid_File").val(data.Value);
            $.messager.alert("提示消息", data.Message);
        },
        error: function (e) {
            $.messager.alert("提示消息", JSON.stringify(e));
        }
    });
}
//function fileupload1() {
//    var fileinput = $("input:last", $("#fileupload1").next("span"));
//    var formData = new FormData();
//    formData.append('file', fileinput[0].files[0]);
//    $.ajax({
//        url: "/Common/UploadImage",
//        type: "post",
//        data: formData,
//        processData: false, //告诉jQuery不要去处理发送的数据
//        contentType: false, //告诉jQuery不要去设置Content-Type请求头
//        success: function (data) {
//            if (data.IsOK)
//                $("#hid_File1").val(data.Value);
//            $.messager.alert("提示消息", data.Message);
//        },
//        error: function (e) {
//            $.messager.alert("提示消息", JSON.stringify(e));
//        }
//    });
//}