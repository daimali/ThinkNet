﻿//不允许关闭的标签的标题
var noCloseTabTitle = "首页";

$(function () {
    //左边菜单
    InitLeftMenu();
    //选项卡菜单
    TabRightMenu();

    //初始化右键菜单
    $('#rightmenu').menu({
        onClick: function (item) {
            RightMenuCloseTab(item.id);
        }
    });

    //选择TAB时刷新内容
    //$('#tabs').tabs({
    //    onSelect: function (title) {
    //        var currTab = $('#tabs').tabs('getTab', title);
    //        var iframe = $(currTab.panel('options').content);

    //        var src = iframe.attr('src');
    //        if (src)
    //            $('#tabs').tabs('update', { tab: currTab, options: { content: createFrame(src) } });

    //    }
    //});
});

var left_menus = {
    "menus": [{
        "menuid": "1",
        "icon": "icon-sys",
        "menuname": "系统管理",
        "menus": [{
            "menuid": "11",
            "menuname": "用户列表",
            "icon": "icon-role",
            "url": "/Login/SysUserList"
        },
        {
            "menuid": "12",
            "menuname": "个人信息",
            "icon": "icon-users",
            "url": "/Login/SysUserInfo"
        },
        {
            "menuid": "13",
            "menuname": "登录日志",
            "icon": "icon-log",
            "url": "/Login/LoginLogList"
        }]
    },
    {
        "menuid": "2",
        "icon": "icon-sys",
        "menuname": "教师管理",
        "menus": [{
            "menuid": "21",
            "menuname": "课程分类",
            "icon": "icon-nav",
            "url": "/Login/SubjectList"
        },
        {
            "menuid": "22",
            "menuname": "所有上课时间",
            "icon": "icon-nav",
            "url": "/Login/TimeSpanList"
        },
        {
            "menuid": "23",
            "menuname": "教师列表",
            "icon": "icon-nav",
            "url": "/Login/TeacherList"
        },
        {
            "menuid": "24",
            "menuname": "教师空闲时间",
            "icon": "icon-nav",
            "url": "/Login/FreeTimeList"
        }]
    }]
};

//初始化左侧菜单
function InitLeftMenu() {
    $("#nav").accordion({ animate: false, fit: true, border: false });
    var defaultTabTitle = '';
    $.each(left_menus.menus, function (i, n) {
        //菜单html绑定
        var menulist = '';
        menulist += '<ul class="navlist">';
        $.each(n.menus, function (j, o) {
            menulist += '<li><div ><a ref="' + o.menuid + '" href="javascript:" rel="' + o.url + '" ><span class="icon ' + o.icon + '" >&nbsp;</span><span class="nav">' + o.menuname + '</span></a></div> ';
            //三级分类绑定
            if (o.child && o.child.length > 0) {
                menulist += '<ul class="third_ul">';
                $.each(o.child, function (k, p) {
                    menulist += '<li><div><a ref="' + p.menuid + '" href="javascript:" rel="' + p.url + '" ><span class="icon ' + p.icon + '" >&nbsp;</span><span class="nav">' + p.menuname + '</span></a></div> </li>'
                });
                menulist += '</ul>';
            }
            menulist += '</li>';
        });
        menulist += '</ul>';
        //动态添加选项卡
        $('#nav').accordion('add', {
            title: n.menuname,
            content: menulist,
            border: false,
            iconCls: 'icon ' + n.icon
        });

        if (i == 0)
        {
            defaultTabTitle = n.menuname;
        }
    });

    //打开第一个下拉框
    $('#nav').accordion('select', defaultTabTitle);

    //左侧菜单点击事件
    $('.navlist li a').click(function () {
        var tabTitle = $(this).children('.nav').text();

        var url = $(this).attr("rel");
        var menuid = $(this).attr("ref");
        var icon = $(this).find('.icon').attr('class');

        var third = find(menuid);
        if (third && third.child && third.child.length > 0) {
            $('.third_ul').slideUp();

            var ul = $(this).parent().next();
            if (ul.is(":hidden"))
                ul.slideDown();
            else
                ul.slideUp();
        }
        else {
            OpenTab(tabTitle, url, icon);
            $('.navlist li div').removeClass("selected");
            $(this).parent().addClass("selected");
        }
    }).hover(function () {
        $(this).parent().addClass("hover");
    }, function () {
        $(this).parent().removeClass("hover");
    });
}

function find(menuid) {
    var obj = null;
    $.each(left_menus.menus, function (i, n) {
        $.each(n.menus, function (j, o) {
            if (o.menuid == menuid) {
                obj = o;
            }
        });
    });

    return obj;
}

//创建打开或者选中Tab窗口
function OpenTab(subtitle, url, icon) {
    if (!$('#tabs').tabs('exists', subtitle)) {
        $('#tabs').tabs('add', {
            title: subtitle,
            content: createFrame(url),
            closable: true,
            icon: icon
        });
    } else {
        $('#tabs').tabs('select', subtitle);
        $('#rightmenu-tabupdate').click();
    }
    TabRightMenu();
}

//创建iframe
function createFrame(url) {
    var s = '<iframe width="100%" height="100%" frameborder="0"  src="' + url + '"></iframe>';
    //var s = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>';
    return s;
}

//为选项卡绑定右键菜单
function TabRightMenu() {
    /*双击关闭TAB选项卡*/
    $(".tabs-inner").dblclick(function () {
        var subtitle = $(this).children(".tabs-closable").text();
        $('#tabs').tabs('close', subtitle);
    });
    /*为选项卡绑定右键*/
    $(".tabs-inner").bind('contextmenu', function (e) {
        $('#rightmenu').menu('show', {
            left: e.pageX,
            top: e.pageY
        });

        var subtitle = $(this).children(".tabs-closable").text();
        $('#rightmenu').data("currtab", subtitle);
        $('#tabs').tabs('select', subtitle);
        return false;
    });
}

//右键关闭选项卡
function RightMenuCloseTab(action) {
    var alltabs = $('#tabs').tabs('tabs');
    var currentTab = $('#tabs').tabs('getSelected');
    var allTabtitle = [];
    $.each(alltabs, function (i, n) {
        allTabtitle.push($(n).panel('options').title);
    });
    switch (action) {
        case "refresh":
            var iframe = $(currentTab.panel('options').content);
            var src = iframe.attr('src');
            $('#tabs').tabs('update', {
                tab: currentTab,
                options: {
                    content: createFrame(src)
                }
            });
            break;
        case "close":
            var currtab_title = currentTab.panel('options').title;
            $('#tabs').tabs('close', currtab_title);
            break;
        case "closeall":
            $.each(allTabtitle, function (i, n) {
                if (n != noCloseTabTitle) {
                    $('#tabs').tabs('close', n);
                }
            });
            break;
        case "closeother":
            var currtab_title = currentTab.panel('options').title;
            $.each(allTabtitle, function (i, n) {
                if (n != currtab_title && n != noCloseTabTitle) {
                    $('#tabs').tabs('close', n);
                }
            });
            break;
        case "closeright":
            var tabIndex = $('#tabs').tabs('getTabIndex', currentTab);
            if (tabIndex == alltabs.length - 1) {
                alert('右边没有选项卡了！');
                return false;
            }
            $.each(allTabtitle, function (i, n) {
                if (i > tabIndex) {
                    if (n != noCloseTabTitle) {
                        $('#tabs').tabs('close', n);
                    }
                }
            });
            break;
        case "closeleft":
            var tabIndex = $('#tabs').tabs('getTabIndex', currentTab);
            if (tabIndex == 1) {
                alert('左边没有选项卡了！');
                return false;
            }
            $.each(allTabtitle, function (i, n) {
                if (i < tabIndex) {
                    if (n != noCloseTabTitle) {
                        $('#tabs').tabs('close', n);
                    }
                }
            });
            break;
        case "exit":
            $('#closeMenu').menu('hide');
            break;
    }
}