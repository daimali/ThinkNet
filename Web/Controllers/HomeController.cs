﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;

namespace ThinkNet.Web.Controllers
{
    public class HomeController : Controller
    {

        #region 后台首页
        [DllImport("kernel32")]
        public static extern void GlobalMemoryStatus(ref MEMORY_INFO meminfo);
        //定义内存的信息结构

        [StructLayout(LayoutKind.Sequential)]
        public struct MEMORY_INFO
        {
            public uint dwLength;
            public uint dwMemoryLoad;
            public uint dwTotalPhys;
            public uint dwAvailPhys;
            public uint dwTotalPageFile;
            public uint dwAvailPageFile;
            public uint dwTotalVirtual;
            public uint dwAvailVirtual;
        }
        public ActionResult Index()
        {
            TimeSpan stime = DateTime.Now.TimeOfDay;
            ViewBag.servername = Server.MachineName;
            ViewBag.serverms = Environment.OSVersion.ToString();
            ViewBag.serverip = Request.ServerVariables["LOCAL_ADDR"];
            ViewBag.server_name = Request.ServerVariables["http_host"];
            ViewBag.serversoft = Request.ServerVariables["server_software"];
            ViewBag.servernet = Environment.Version.Major + "." + Environment.Version.Minor + "." + Environment.Version.Build + "." + Environment.Version.Revision;
            ViewBag.serverhttps = Request.ServerVariables["HTTPS"];
            ViewBag.serverport = Request.ServerVariables["server_port"];
            ViewBag.serverout = Server.ScriptTimeout.ToString();
            ViewBag.servertime = DateTime.Now.ToString();
            ViewBag.serverarea = (DateTime.Now - DateTime.UtcNow).TotalHours > 0 ? "+" + (DateTime.Now - DateTime.UtcNow).TotalHours.ToString() : (DateTime.Now - DateTime.UtcNow).TotalHours.ToString();
            try
            {
                ViewBag.aspnetn = (System.Diagnostics.Process.GetCurrentProcess().WorkingSet64 / 1048576).ToString("N2") + " MB";
            }
            catch
            {
                ViewBag.aspnetn = "系统拒绝提供。";
            }
            try
            {
                ViewBag.aspnetcpu = (System.Diagnostics.Process.GetCurrentProcess().TotalProcessorTime).TotalSeconds.ToString("N0") + " 秒";
            }
            catch
            {
                ViewBag.aspnetcpu = "系统拒绝提供。";
            }
            ViewBag.serverstart = (System.Environment.TickCount / 3600000).ToString("N2");
            try
            {
                ViewBag.prstart = System.Diagnostics.Process.GetCurrentProcess().StartTime.ToString();
            }
            catch
            {
                ViewBag.prstart = "系统拒绝提供。";
            }
            ViewBag.cpuc = Environment.GetEnvironmentVariable("NUMBER_OF_PROCESSORS");
            ViewBag.cputype = Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER");

            //调用GlobalMemoryStatus函数获取内存的相关信息

            MEMORY_INFO MemInfo;
            MemInfo = new MEMORY_INFO();
            GlobalMemoryStatus(ref MemInfo);

            ViewBag.LbdwMemoryLoad = MemInfo.dwMemoryLoad + " %";
            ViewBag.LbdwTotalPhys = dFileSize(MemInfo.dwTotalPhys);
            ViewBag.LbdwAvailPhys = dFileSize(MemInfo.dwAvailPhys);
            ViewBag.LbdwTotalPageFile = dFileSize(MemInfo.dwTotalPageFile);
            ViewBag.LbdwAvailPageFile = dFileSize(MemInfo.dwAvailPageFile);
            ViewBag.LbdwTotalVirtual = dFileSize(MemInfo.dwTotalVirtual);


            ViewBag.serverppath = Request.ServerVariables["APPL_PHYSICAL_PATH"];
            ViewBag.servernpath = Request.ServerVariables["path_translated"];
            ViewBag.servers = Session.Contents.Count.ToString();
            //ViewBag.servera = Application.Contents.Count.ToString();

            TimeSpan etime = DateTime.Now.TimeOfDay;
            TimeSpan t = etime.Subtract(stime);
            ViewBag.runtime = t.TotalMilliseconds.ToString();
            return View();
        }
        private static string dFileSize(double FileSize)
        {
            if (FileSize < 1024)
            {
                return FileSize + " B";
            }
            else if ((FileSize < 1024 * 1024))
            {
                return Math.Round((FileSize / 1024), 2) + " KB";
            }
            else
            {
                return Math.Round(FileSize / (1024 * 1024), 2) + " MB";
            }
        }

        #endregion
    }
}