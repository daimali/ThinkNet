﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ThinkNet.Web.Controllers
{
    public class LoginController : Controller
    {
        [AllowAnonymous, HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous, HttpPost]
        public JsonResult LoginIn(string logname, string logpass, string logcode)
        {
            //object validCode = Session["ValidateCode"];
            //var result = SystemUserBLL.Login(logname, logpass, logcode, validCode);
            ////记录登录日志
            //LoginLogBLL.LoginLogAdd(logname, result.IsOK);
            var result = new { IsOK = true, Message = "登录成功" };
            return Json(result);
        }

        [AllowAnonymous, HttpGet]
        public ActionResult ValidateCode()
        {
            //string text = PublicFunction.CreateVerificationText(4);
            //Session["ValidateCode"] = text;
            //byte[] fileContents = WebUtility.ValidCodeImage(text);
            //return File(fileContents, "image/jpeg");
            //return File(null, "image/jpeg");
            return null;
        }

        public ActionResult LoginOut()
        {
            //退出登录
            return Redirect("/Login/Login");
        }
    }
}