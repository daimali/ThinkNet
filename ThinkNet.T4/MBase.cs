﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThinkNet.T4
{
    /// <summary>
    /// 公用字段
    /// </summary>
    public class MBase
    {
        public MBase()
        { }
        public virtual DateTime? InsertTime { set; get; }
        public virtual DateTime? UpdateTime { set; get; }
        public virtual DateTime? DeleteTime { set; get; }
        public virtual long? Mark { set; get; }
    }
}
