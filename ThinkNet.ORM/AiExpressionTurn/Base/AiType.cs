﻿using System;
using System.Collections.Generic;

namespace ThinkNet.ORM
{
    public static class AiType
    {
        /// <summary>
        /// in查询：例（select * from tblDemo where id in(1, 2, 3)）
        /// </summary>
        /// <param name="my"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool In(this object my, object obj)
        {
            return true;
        }
        public static bool NotIn(this object my, object obj)
        {
            return true;
        }

        /// <summary>
        /// in查询：例（select * from tblDemo where id in(1, 2, 3)）
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="my"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool In<T>(this object my, List<T> obj)
        {
            return true;
        }
        public static bool NotIn<T>(this object my, List<T> obj)
        {
            return true;
        }

        /// <summary>
        /// in查询：例（select * from tblDemo where id in(1, 2, 3)）
        /// </summary>
        /// <param name="my"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool In(this object my, params dynamic[] obj)
        {
            return true;
        }
        public static bool NotIn(this object my, params dynamic[] obj)
        {
            return true;
        }

        /// <summary>
        /// like模糊查询：例（select * from tblDemo where name like '%yellow%'）
        /// </summary>
        /// <param name="my"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool Like(this object my, string obj)
        {
            return true;
        }

        public static bool NotLike(this object my, string obj)
        {
            return true;
        }
        
        public static T? ConvertTo<T>(this IConvertible convertibleValue) where T : struct
        {
            if (null == convertibleValue)
            {
                return null;
            }
            return (T?)Convert.ChangeType(convertibleValue, typeof(T));
        }
        
    }
}
