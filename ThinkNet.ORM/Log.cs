﻿using System;
using System.Configuration;
using System.IO;

namespace ThinkNet.ORM
{
    public class Log
    {
        public static void Save(string sql)
        {
            try
            {
                var saveLog = ConfigurationManager.AppSettings[Config.BaseConfig.DefaultSaveLogKey];

                var savePath = ConfigurationManager.AppSettings[Config.BaseConfig.DefaultSavePathKey];

                if (!string.IsNullOrWhiteSpace(saveLog) && saveLog.ToLower() == "true")
                {
                    if (string.IsNullOrWhiteSpace(savePath))
                        savePath = "D:\\Log\\";
                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }
                    System.IO.File.AppendAllText(savePath + DateTime.Now.ToString("yyyyMMdd") + ".txt", DateTime.Now.ToString() + ":\r\n" + sql + "\r\n");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        
    }
}
