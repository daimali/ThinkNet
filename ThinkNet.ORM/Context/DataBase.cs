﻿using ThinkNet.ORM.DataBase;

namespace ThinkNet.ORM.Context
{
    public sealed class DataBase
    {
        public static IDataBase Instance
        {
            get { return SingletonCreator.instance; }
        }
        private class SingletonCreator
        {
            internal static string assName = "ThinkNet.ORM";

            internal static readonly IDataBase instance = Base.CreateInstance<IDataBase>(assName, assName + ".DataBase", Config.BaseConfig.DataBase);
        }
    }
}
