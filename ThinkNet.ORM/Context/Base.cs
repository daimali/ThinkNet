﻿using System.Reflection;

namespace ThinkNet.ORM.Context
{
    public class Base
    {
        /// <summary>
        /// 创建对象实例
        /// </summary>
        /// <typeparam name="T">要创建对象的类型</typeparam>
        /// <param name="assemblyName">类型所在程序集名称</param>
        /// <param name="nameSpace">类型所在命名空间</param>
        /// <param name="className">类型名</param>
        /// <returns></returns>
        public static T CreateInstance<T>(string assemblyName, string nameSpace, string className)
        {
            try
            {
                //命名空间.类型名
                string fullName = nameSpace + "." + className;
                //加载程序集，创建程序集里面的 命名空间.类型名 实例
                object ect = Assembly.Load(assemblyName).CreateInstance(fullName);
                //类型转换并返回
                return (T)ect;
            }
            catch
            {
                //发生异常，返回类型的默认值
                return default(T);
            }
        }
    }
}
