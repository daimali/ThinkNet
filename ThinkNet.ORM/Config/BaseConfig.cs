﻿namespace ThinkNet.ORM.Config
{
    public class BaseConfig
    {
        //反射的类名称
        public static readonly string DataBase = "SqlServer";

        #region AppSettings Key名称设置

        /// <summary>
        /// （AppSettings）默认的数据库连接字符串KEY
        /// </summary>
        public static readonly string DefaultDBKey = "SqlConnectionString";

        /// <summary>
        /// （AppSettings）是否把SQL记录到文本日志
        /// </summary>
        public static readonly string DefaultSaveLogKey = "SaveLog";

        /// <summary>
        /// （AppSettings）文本日志路径
        /// </summary>
        public static readonly string DefaultSavePathKey = "SavePath";

        #endregion
    }
}
