﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Transactions;

namespace ThinkNet.ORM.DataBase
{
    public abstract class IDataBase
    {
        public abstract int Insert<T>(T t, bool IsReturnId = false);

        public abstract long Insert64<T>(T t, bool IsReturnId = false);
        public abstract int InsertBat<T>(List<T> t);

        public abstract int Update<T>(T t);

        public abstract int Update<T>(T t, Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp);

        public abstract int UpdateBat<T>(List<T> t);

        public abstract int LogicalDelete<T>(T t);

        public abstract int LogicalDelete<T>(T t, Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp);

        public abstract int Delete<T>(T t);

        public abstract int Delete<T>(T t, Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp);

        public abstract T Get<T>(string id, string showCols = "*", bool nolock = true);

        public abstract NT Get<T, NT>(string id, string showCols = "*", bool nolock = true);

        public abstract IList<T> Get<T>(Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp, string showCols = "*", bool nolock = true);

        public abstract IList<NT> Get<T, NT>(Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp, string showCols = "*", bool nolock = true);

        public abstract IList<T> GetWhere<T>(string where, string orderBy = "", string showCols = "*", object param = null, bool nolock = true);

        public abstract IList<NT> GetWhere<T, NT>(string where, string orderBy, string showCols = "*", object param = null, bool nolock = true);

        public abstract MPageData<T> Get<T>(Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp, int pageIndex, int pageSize, string showCols = "*", bool nolock = true, ESqlVersion sqlVersion = ESqlVersion.SqlServer2012);

        public abstract MPageData<NT> Get<T, NT>(Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp, int pageIndex, int pageSize, string showCols = "*", bool nolock = true, ESqlVersion sqlVersion = ESqlVersion.SqlServer2012);

        public abstract MPageData<T> GetWhere<T>(string where, string orderBy, int pageIndex, int pageSize, string showCols = "*", object param = null, bool nolock = true, ESqlVersion sqlVersion = ESqlVersion.SqlServer2012);

        public abstract MPageData<NT> GetWhere<T, NT>(string where, string orderBy, int pageIndex, int pageSize, string showCols = "*", object param = null, bool nolock = true, ESqlVersion sqlVersion = ESqlVersion.SqlServer2012);

        public abstract TransactionScope TransMaster(IsolationLevel isolationLevel = IsolationLevel.Serializable);

        public abstract IList<T> RunProc<T>(string procName, object param = null);

        public abstract IList<T> RunSql<T>(string sql, object param = null);

        public abstract MPageData<T> RunSql<T>(string sql, string orderBy, int pageIndex, int pageSize, object param = null, ESqlVersion sqlVersion = ESqlVersion.SqlServer2012);

        public abstract decimal Sum<T>(Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp, string SumCol, bool nolock = true);

        public abstract int Count<T>(Expression<Func<IQueryable<T>, IQueryable<T>>> bizExp, bool nolock = true);
    }
}
