﻿using Dapper;

namespace ThinkNet.ORM
{
    public class MSqlData
    {
        public string sql { get; set; }

        public DynamicParameters param { get; set; }
    }
}
