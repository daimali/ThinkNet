﻿using System.Collections.Generic;

namespace ThinkNet.ORM
{
    //分页查询模型
    public class MPageData<T>
    {
        public int total { get; set; }

        public IList<T> rows { get; set; }
    }
}
