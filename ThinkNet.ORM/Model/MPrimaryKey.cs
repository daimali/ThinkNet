﻿namespace ThinkNet.ORM
{
    //实体返回主键信息
    public class MPrimaryKey
    {
        public string KeyName { get; set; }

        public bool IsIncrement { get; set; }
    }
}
