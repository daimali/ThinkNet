﻿namespace ThinkNet.ORM
{
    //分页查询数据库版本
    public enum ESqlVersion
    {
        SqlServer2005,
        SqlServer2008,
        SqlServer2012
    }
}
