﻿using System;

namespace ThinkNet.ORM
{
    /// <summary>
    /// 自动对系统字段的填充
    /// </summary>
    public static class FillEntity
    {
        public static void SetInsertSysCols<T>(this T entity)
        {
            var createDate = entity.GetType().GetProperty("InsertTime");
            if (createDate != null && createDate.GetValue(entity, null) == null)
                createDate.SetValue(entity, DateTime.Now, null);

            var updateDate = entity.GetType().GetProperty("UpdateTime");
            if (updateDate != null && updateDate.GetValue(entity, null) == null)
                updateDate.SetValue(entity, DateTime.Now, null);

            var deleteDate = entity.GetType().GetProperty("DeleteTime");
            if (deleteDate != null && deleteDate.GetValue(entity, null) == null)
                deleteDate.SetValue(entity, Convert.ToDateTime("1900-01-01"), null);

            var mark = entity.GetType().GetProperty("Mark");
            if (mark != null && mark.GetValue(entity, null) == null)
                mark.SetValue(entity, 1, null);

            var version = entity.GetType().GetProperty("Version");
            if (version != null && version.GetValue(entity, null) == null)
                version.SetValue(entity, 0L, null);
        }
        public static void SetUpdateSysCols<T>(this T entity)
        {
            var updateDate = entity.GetType().GetProperty("UpdateTime");
            if (updateDate != null && updateDate.GetValue(entity, null) == null)
                updateDate.SetValue(entity, DateTime.Now, null);

            var mark = entity.GetType().GetProperty("Mark");
            if (mark != null && mark.GetValue(entity, null) == null)
                mark.SetValue(entity, 2, null);
        }
        public static void SetDeleteSysCols<T>(this T entity)
        {
            var deleteDate = entity.GetType().GetProperty("DeleteTime");
            if (deleteDate != null && deleteDate.GetValue(entity, null) == null)
                deleteDate.SetValue(entity, DateTime.Now, null);

            var mark = entity.GetType().GetProperty("Mark");
            if (mark != null && mark.GetValue(entity, null) == null)
                mark.SetValue(entity, 0, null);
        }
    }
}
